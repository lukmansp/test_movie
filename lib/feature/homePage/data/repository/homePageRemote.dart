import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:test_interview/feature/homePage/data/model/movie.dart';
import 'package:test_interview/feature/homePage/data/model/quote.dart';
import 'package:test_interview/feature/homePage/data/repository/ListMovie.dart';

abstract class HomePageRepository {
  Future<Quote> fetchQuote();
  List<Movie> getListMovie();
}

class HomePageRepositoryImpl extends HomePageRepository {
  final dio = Dio();
  @override
  Future<Quote> fetchQuote() async {
    final response = await dio.get("https://some-random-api.ml/animu/quote");
    final responseJson = jsonDecode(response.data);
    return Quote.fromJson(responseJson);
  }

  @override
  List<Movie> getListMovie() {
    return MockUpListMovie().listMovie;
  }
}
