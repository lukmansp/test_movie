import 'package:test_interview/feature/homePage/data/model/movie.dart';

class MockUpListMovie {
  List<Movie> listMovie = [
    Movie(
        id: 1,
        title: "Stardust",
        year: 1998,
        runTime: "92",
        genres: ["Comedy", "Fantasy"],
        director: "Tim Burton",
        actors: "Alec Baldwin, Geena Davis, Annie McEnroe, Maurice Page",
        summary:
            "In a countryside town bordering on a magical land, a young man makes a promise to his beloved that he'll retrieve a fallen star by venturing into the magical realm.",
        postedUrl:
            "https://images-na.ssl-images-amazon.com/images/M/MV5BMjkyMTE1OTYwNF5BMl5BanBnXkFtZTcwMDIxODYzMw@@._V1_SX300.jpg"),
    Movie(
        id: 2,
        title: "The Cotton Club",
        year: 1998,
        runTime: "92",
        genres: ["Crime", "Drama", "Music"],
        director: "Francis Ford Coppola",
        actors: "Richard Gere, Gregory Hines, Diane Lane, Lonette McKee",
        summary:
            "The Cotton Club was a famous night club in Harlem. The story follows the people that visited the club, those that ran it, and is peppered with the Jazz music that made it so famous.",
        postedUrl:
            "https://images-na.ssl-images-amazon.com/images/M/MV5BMTU5ODAyNzA4OV5BMl5BanBnXkFtZTcwNzYwNTIzNA@@._V1_SX300.jpg"),
    Movie(
        id: 3,
        title: "The Shawshank Redemption",
        year: 1994,
        runTime: "142",
        genres: ["Crime", "Drama"],
        director: "Frank Darabont",
        actors: "Tim Robbins, Morgan Freeman, Bob Gunton, William Sadler",
        summary:
            "Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.",
        postedUrl:
            "https://images-na.ssl-images-amazon.com/images/M/MV5BNGQxNDgzZWQtZTNjNi00M2RkLWExZmEtNmE1NjEyZDEwMzA5XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg"),
    Movie(
        id: 4,
        title: "Crocodile Dundee",
        year: 1986,
        runTime: "97",
        genres: ["Adventure", "Comedy"],
        director: "Peter Faiman",
        actors: "Paul Hogan, Linda Kozlowski, John Meillon, David Gulpilil",
        summary:
            "An American reporter goes to the Australian outback to meet an eccentric crocodile poacher and invites him to New York City.",
        postedUrl:
            "https://images-na.ssl-images-amazon.com/images/M/MV5BMTg0MTU1MTg4NF5BMl5BanBnXkFtZTgwMDgzNzYxMTE@._V1_SX300.jpg"),
    Movie(
        id: 5,
        title: "Valkyrie",
        year: 2008,
        runTime: "121",
        genres: ["Drama", "History", "Thriller"],
        director: "Bryan Singer",
        actors: "Tom Cruise, Kenneth Branagh, Bill Nighy, Tom Wilkinson",
        summary:
            "A dramatization of the 20 July assassination and political coup summary by desperate renegade German Army officers against Hitler during World War II.",
        postedUrl:
            "http://ia.media-imdb.com/images/M/MV5BMTg3Njc2ODEyN15BMl5BanBnXkFtZTcwNTAwMzc3NA@@._V1_SX300.jpg"),
    Movie(
        id: 6,
        title: "Ratatouille",
        year: 2007,
        runTime: "111",
        genres: ["Animation", "Comedy", "Family"],
        director: "Brad Bird, Jan Pinkava",
        actors: "Patton Oswalt, Ian Holm, Lou Romano, Brian Dennehy",
        summary:
            "A rat who can cook makes an unusual alliance with a young kitchen worker at a famous restaurant.",
        postedUrl:
            "https://images-na.ssl-images-amazon.com/images/M/MV5BMTMzODU0NTkxMF5BMl5BanBnXkFtZTcwMjQ4MzMzMw@@._V1_SX300.jpg")
  ];
}
