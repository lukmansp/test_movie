class Movie {
  int id;
  String title;
  int year;
  String runTime;
  List genres;
  String director;
  String actors;
  String summary;
  String postedUrl;
  Movie(
      {required this.id,
      required this.title,
      required this.year,
      required this.runTime,
      required this.genres,
      required this.director,
      required this.actors,
      required this.summary,
      required this.postedUrl});
}
