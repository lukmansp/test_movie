import 'dart:convert';

Quote quoteFromJson(String str) => Quote.fromJson(json.decode(str));

String quoteToJson(Quote data) => json.encode(data.toJson());

class Quote {
  String? sentence;
  String? character;
  String? anime;

  Quote({
    this.sentence,
    this.character,
    this.anime,
  });

  factory Quote.fromJson(Map<String, dynamic> json) => Quote(
        sentence: json["sentence"],
        character: json["character"],
        anime: json["anime"],
      );

  Map<String, dynamic> toJson() => {
        "sentence": sentence,
        "character": character,
        "anime": anime,
      };
}
