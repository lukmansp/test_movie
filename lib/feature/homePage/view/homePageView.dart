import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:test_interview/feature/homePage/view/component/AddPopUpMovie.dart';
import 'package:test_interview/feature/homePage/view/component/cardMovie.dart';
import 'package:test_interview/feature/homePage/view/ViewModel/HomePageViewModel.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final HomePageViewModel _viewModel = HomePageViewModel();

  @override
  void initState() {
    _viewModel.initListMovie();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("My Movie"),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 13),
            child: TextField(
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'search movie',
              ),
              onChanged: (value) {
                _viewModel.searchMovie(value);
              },
            ),
          ),
          Expanded(
              child: Container(
            margin: const EdgeInsets.only(left: 8, right: 8, bottom: 10),
            child: Observer(builder: (context) {
              return _viewModel.isMovieEmpty
                  ? const Text("Can't Found Movie")
                  : ListView.builder(
                      itemCount: _viewModel.listMovie.length,
                      itemBuilder: (context, index) {
                        return CardMovie(
                            urlImage: _viewModel.listMovie[index].postedUrl,
                            title: _viewModel.listMovie[index].title,
                            director: _viewModel.listMovie[index].director,
                            genre: _viewModel.listMovie[index].genres);
                      },
                    );
            }),
          ))
        ],
      ),
      floatingActionButton: AddPopUpMovie(
        viewModel: _viewModel,
      ),
    );
  }
}
