import 'package:flutter/material.dart';

class CardMovie extends StatelessWidget {
  final String title;
  final String director;
  final List genre;
  final String urlImage;
  const CardMovie(
      {super.key,
      required this.title,
      required this.director,
      required this.genre,
      required this.urlImage});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(7),
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(7),
          boxShadow: [
            BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                blurRadius: 3,
                offset: Offset(0.2, 0.2))
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            alignment: Alignment.center,
            child: Image.network(
              urlImage,
              errorBuilder: (context, error, stackTrace) {
                return Image.asset(
                  "lib/helper/asset/empty.png",
                  height: 200,
                  width: 200,
                  fit: BoxFit.contain,
                );
              },
              loadingBuilder: (context, child, loadingProgress) {
                return loadingProgress == null
                    ? child
                    : CircularProgressIndicator();
              },
              height: 200,
              width: 200,
              fit: BoxFit.contain,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Text(
            title,
            style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
          ),
          Text(director),
          Container(
              alignment: Alignment.bottomRight, child: Text("${genre[0]}"))
        ],
      ),
    );
  }
}
