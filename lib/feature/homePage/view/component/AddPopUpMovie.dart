import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:test_interview/feature/homePage/view/ViewModel/HomePageViewModel.dart';

class AddPopUpMovie extends StatefulWidget {
  final HomePageViewModel viewModel;
  AddPopUpMovie({Key? key, required this.viewModel}) : super(key: key);

  @override
  State<AddPopUpMovie> createState() => _AddPopUpMovieState();
}

class _AddPopUpMovieState extends State<AddPopUpMovie> {
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        widget.viewModel.removeAllMovie();
        widget.viewModel.searchMovie('');
        showDialog<String>(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: const Text('Add Movie'),
                contentPadding: EdgeInsets.all(12),
                content: SingleChildScrollView(
                  child: SizedBox(
                    width: double.maxFinite,
                    child: Observer(builder: (context) {
                      return Column(
                        children: [
                          TextFormField(
                            autofocus: true,
                            onChanged: (value) {
                              widget.viewModel.erroAddMovie = false;
                              widget.viewModel.urlImageMovie = value;
                            },
                            decoration: const InputDecoration(
                                contentPadding: EdgeInsets.all(10),
                                isDense: true,
                                border: OutlineInputBorder(),
                                hintText: "Insert url image movie"),
                          ),
                          Container(
                              margin: EdgeInsets.symmetric(vertical: 7),
                              child: TextFormField(
                                onChanged: (value) {
                                  widget.viewModel.erroAddMovie = false;
                                  widget.viewModel.titleMovie = value;
                                  print(widget.viewModel.titleMovie);
                                },
                                decoration: const InputDecoration(
                                    contentPadding: EdgeInsets.all(10),
                                    isDense: true,
                                    border: OutlineInputBorder(),
                                    hintText: "Insert title movie"),
                              )),
                          TextFormField(
                            onChanged: (value) {
                              widget.viewModel.erroAddMovie = false;
                              widget.viewModel.directorMovie = value;
                            },
                            decoration: const InputDecoration(
                                contentPadding: EdgeInsets.all(10),
                                isDense: true,
                                border: OutlineInputBorder(),
                                hintText: "Insert director movie"),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            height: 40,
                            child: ListView(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              children: [
                                for (var item in widget.viewModel.listGenre)
                                  GestureDetector(
                                    onTap: () {
                                      widget.viewModel.selectedListGenre = [
                                        item
                                      ];
                                    },
                                    child: Card(
                                      color: item ==
                                              widget.viewModel
                                                  .selectedListGenre[0]
                                          ? Colors.blueAccent
                                          : Colors.white,
                                      child: Container(
                                          padding: EdgeInsets.all(7),
                                          child: Text(item)),
                                    ),
                                  )
                              ],
                            ),
                          ),
                          widget.viewModel.erroAddMovie
                              ? const Text(
                                  'Insert all form!',
                                  style: TextStyle(
                                      color: Colors.red,
                                      fontSize: 11,
                                      fontWeight: FontWeight.normal),
                                )
                              : Container(),
                        ],
                      );
                    }),
                  ),
                ),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context, 'Cancel'),
                    child: const Text('Cancel'),
                  ),
                  TextButton(
                    onPressed: () {
                      widget.viewModel.addMovie(context);
                    },
                    child: const Text('OK'),
                  )
                ],
              );
            });
      },
      child: const Icon(Icons.add),
    );
  }
}
