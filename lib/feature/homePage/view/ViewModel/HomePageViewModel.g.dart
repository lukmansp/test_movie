// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'HomePageViewModel.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$HomePageViewModel on HomePageViewModelInterface, Store {
  late final _$listMovieAtom =
      Atom(name: 'HomePageViewModelInterface.listMovie', context: context);

  @override
  ObservableList<Movie> get listMovie {
    _$listMovieAtom.reportRead();
    return super.listMovie;
  }

  @override
  set listMovie(ObservableList<Movie> value) {
    _$listMovieAtom.reportWrite(value, super.listMovie, () {
      super.listMovie = value;
    });
  }

  late final _$listAddedMovieAtom =
      Atom(name: 'HomePageViewModelInterface.listAddedMovie', context: context);

  @override
  ObservableList<Movie> get listAddedMovie {
    _$listAddedMovieAtom.reportRead();
    return super.listAddedMovie;
  }

  @override
  set listAddedMovie(ObservableList<Movie> value) {
    _$listAddedMovieAtom.reportWrite(value, super.listAddedMovie, () {
      super.listAddedMovie = value;
    });
  }

  late final _$isMovieEmptyAtom =
      Atom(name: 'HomePageViewModelInterface.isMovieEmpty', context: context);

  @override
  bool get isMovieEmpty {
    _$isMovieEmptyAtom.reportRead();
    return super.isMovieEmpty;
  }

  @override
  set isMovieEmpty(bool value) {
    _$isMovieEmptyAtom.reportWrite(value, super.isMovieEmpty, () {
      super.isMovieEmpty = value;
    });
  }

  late final _$isAddedMovieAtom =
      Atom(name: 'HomePageViewModelInterface.isAddedMovie', context: context);

  @override
  bool get isAddedMovie {
    _$isAddedMovieAtom.reportRead();
    return super.isAddedMovie;
  }

  @override
  set isAddedMovie(bool value) {
    _$isAddedMovieAtom.reportWrite(value, super.isAddedMovie, () {
      super.isAddedMovie = value;
    });
  }

  late final _$erroAddMovieAtom =
      Atom(name: 'HomePageViewModelInterface.erroAddMovie', context: context);

  @override
  bool get erroAddMovie {
    _$erroAddMovieAtom.reportRead();
    return super.erroAddMovie;
  }

  @override
  set erroAddMovie(bool value) {
    _$erroAddMovieAtom.reportWrite(value, super.erroAddMovie, () {
      super.erroAddMovie = value;
    });
  }

  late final _$listGenreAtom =
      Atom(name: 'HomePageViewModelInterface.listGenre', context: context);

  @override
  List<String> get listGenre {
    _$listGenreAtom.reportRead();
    return super.listGenre;
  }

  @override
  set listGenre(List<String> value) {
    _$listGenreAtom.reportWrite(value, super.listGenre, () {
      super.listGenre = value;
    });
  }

  late final _$selectedListGenreAtom = Atom(
      name: 'HomePageViewModelInterface.selectedListGenre', context: context);

  @override
  List<String> get selectedListGenre {
    _$selectedListGenreAtom.reportRead();
    return super.selectedListGenre;
  }

  @override
  set selectedListGenre(List<String> value) {
    _$selectedListGenreAtom.reportWrite(value, super.selectedListGenre, () {
      super.selectedListGenre = value;
    });
  }

  late final _$urlImageMovieAtom =
      Atom(name: 'HomePageViewModelInterface.urlImageMovie', context: context);

  @override
  String get urlImageMovie {
    _$urlImageMovieAtom.reportRead();
    return super.urlImageMovie;
  }

  @override
  set urlImageMovie(String value) {
    _$urlImageMovieAtom.reportWrite(value, super.urlImageMovie, () {
      super.urlImageMovie = value;
    });
  }

  late final _$titleMovieAtom =
      Atom(name: 'HomePageViewModelInterface.titleMovie', context: context);

  @override
  String get titleMovie {
    _$titleMovieAtom.reportRead();
    return super.titleMovie;
  }

  @override
  set titleMovie(String value) {
    _$titleMovieAtom.reportWrite(value, super.titleMovie, () {
      super.titleMovie = value;
    });
  }

  late final _$directorMovieAtom =
      Atom(name: 'HomePageViewModelInterface.directorMovie', context: context);

  @override
  String get directorMovie {
    _$directorMovieAtom.reportRead();
    return super.directorMovie;
  }

  @override
  set directorMovie(String value) {
    _$directorMovieAtom.reportWrite(value, super.directorMovie, () {
      super.directorMovie = value;
    });
  }

  late final _$HomePageViewModelInterfaceActionController =
      ActionController(name: 'HomePageViewModelInterface', context: context);

  @override
  List<Movie> getListMovie() {
    final _$actionInfo = _$HomePageViewModelInterfaceActionController
        .startAction(name: 'HomePageViewModelInterface.getListMovie');
    try {
      return super.getListMovie();
    } finally {
      _$HomePageViewModelInterfaceActionController.endAction(_$actionInfo);
    }
  }

  @override
  List<Movie> initListMovie() {
    final _$actionInfo = _$HomePageViewModelInterfaceActionController
        .startAction(name: 'HomePageViewModelInterface.initListMovie');
    try {
      return super.initListMovie();
    } finally {
      _$HomePageViewModelInterfaceActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic searchMovie(String input) {
    final _$actionInfo = _$HomePageViewModelInterfaceActionController
        .startAction(name: 'HomePageViewModelInterface.searchMovie');
    try {
      return super.searchMovie(input);
    } finally {
      _$HomePageViewModelInterfaceActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic addMovie(BuildContext context) {
    final _$actionInfo = _$HomePageViewModelInterfaceActionController
        .startAction(name: 'HomePageViewModelInterface.addMovie');
    try {
      return super.addMovie(context);
    } finally {
      _$HomePageViewModelInterfaceActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic removeAllMovie() {
    final _$actionInfo = _$HomePageViewModelInterfaceActionController
        .startAction(name: 'HomePageViewModelInterface.removeAllMovie');
    try {
      return super.removeAllMovie();
    } finally {
      _$HomePageViewModelInterfaceActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
listMovie: ${listMovie},
listAddedMovie: ${listAddedMovie},
isMovieEmpty: ${isMovieEmpty},
isAddedMovie: ${isAddedMovie},
erroAddMovie: ${erroAddMovie},
listGenre: ${listGenre},
selectedListGenre: ${selectedListGenre},
urlImageMovie: ${urlImageMovie},
titleMovie: ${titleMovie},
directorMovie: ${directorMovie}
    ''';
  }
}
