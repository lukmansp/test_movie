import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
import 'package:test_interview/core/locator.dart';
import 'package:test_interview/feature/homePage/data/model/movie.dart';
import 'package:test_interview/feature/homePage/data/repository/homePageRemote.dart';
part 'HomePageViewModel.g.dart';

class HomePageViewModel = HomePageViewModelInterface with _$HomePageViewModel;

abstract class HomePageViewModelInterface with Store {
  @observable
  var listMovie = ObservableList<Movie>();

  @observable
  var listAddedMovie = ObservableList<Movie>();

  @observable
  bool isMovieEmpty = true;

  @observable
  bool isAddedMovie = false;

  @observable
  bool erroAddMovie = false;

  @observable
  List<String> listGenre = ['Action', 'Drama', 'Commedy', 'Horor', 'Anime'];

  @observable
  List<String> selectedListGenre = [];

  @observable
  String urlImageMovie = '';

  @observable
  String titleMovie = '';

  @observable
  String directorMovie = '';

  @action
  List<Movie> getListMovie() {
    final list = locator.get<HomePageRepository>().getListMovie();
    return list;
  }

  @action
  List<Movie> initListMovie() {
    isMovieEmpty = false;
    if (isAddedMovie) {
      listMovie = listAddedMovie;
      return listMovie;
    } else {
      var list = getListMovie();
      listMovie = list.asObservable();
      return list;
    }
  }

  @action
  searchMovie(String input) {
    var list = initListMovie();
    var result = list.where(
        (element) => element.title.toLowerCase().contains(input.toLowerCase()));
    if (input.isEmpty) {
      isMovieEmpty = false;
      initListMovie();
    } else if (result.isEmpty) {
      isMovieEmpty = true;
      initListMovie();
    } else {
      isMovieEmpty = false;
      listMovie = result.toList().asObservable();
    }
  }

  @action
  addMovie(BuildContext context) {
    if (urlImageMovie.isEmpty ||
        titleMovie.isEmpty ||
        directorMovie.isEmpty ||
        selectedListGenre.first.isEmpty) {
      isAddedMovie = false;
      erroAddMovie = true;
    } else {
      isAddedMovie = true;
      erroAddMovie = false;
      final id = DateTime.now().microsecondsSinceEpoch.toInt();
      listMovie.add(Movie(
          id: id,
          title: titleMovie,
          year: 2022,
          runTime: '129',
          genres: selectedListGenre,
          director: directorMovie,
          actors: 'actors',
          summary: 'summary',
          postedUrl: urlImageMovie));
      listAddedMovie = listMovie;
      Navigator.pop(context);
    }
  }

  @action
  removeAllMovie() {
    erroAddMovie = true;
    listMovie = initListMovie().asObservable();
    urlImageMovie = '';
    titleMovie = '';
    directorMovie = '';
    selectedListGenre = [''];
  }
}
