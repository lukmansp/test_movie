import 'package:flutter/material.dart';
import 'package:test_interview/core/locator.dart';

import '../homePage/data/model/quote.dart';
import '../homePage/view/ViewModel/HomePageViewModel.dart';

class MyWidget extends StatefulWidget {
  const MyWidget({Key? key}) : super(key: key);

  @override
  State<MyWidget> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<MyWidget> {
  Quote? displayQuote;
  Future<void> _changeQuote() async {
    // Quote quote = await locator.get<HomePageViewModel>().getQuote();
    setState(() {
      // displayQuote = quote;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Quote View"),
      ),
      body: Center(
        child: displayQuote == null
            ? const Text("No Quote Available")
            : Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('"${displayQuote?.sentence ?? ""}"',
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                            fontSize: 20, fontStyle: FontStyle.italic)),
                    const SizedBox(height: 10),
                    Text("- ${displayQuote?.character ?? ""}")
                  ],
                ),
              ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _changeQuote,
        tooltip: 'Next',
        child: const Icon(Icons.arrow_forward_ios_rounded),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
