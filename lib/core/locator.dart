import 'package:get_it/get_it.dart';
import 'package:test_interview/feature/homePage/data/repository/homePageRemote.dart';
import 'package:test_interview/feature/homePage/view/ViewModel/HomePageViewModel.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  _registerRepositories();
  _registerService();
  _registerViewModel();
}

void _registerRepositories() {
  locator.registerLazySingleton<HomePageRepository>(
      () => HomePageRepositoryImpl());
}

void _registerService() {}

void _registerViewModel() {
  locator.registerLazySingleton<HomePageViewModel>(() => HomePageViewModel());
}
